package com.example.converterapp.domain.usecases

import com.example.converterapp.domain.entities.ResultConvert
import com.example.converterapp.domain.repository.CurrencyRepository

class GetConvertionResultUseCase{
    private val repository = CurrencyRepository
    suspend fun convert(to: String, from: String, amount: String): ResultConvert {
        return repository.convert(to, from, amount)
    }
}
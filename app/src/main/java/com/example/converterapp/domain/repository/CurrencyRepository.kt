package com.example.converterapp.domain.repository

import com.example.converterapp.data.retrofit.RetrofitInstance
import com.example.converterapp.domain.entities.Rates
import com.example.converterapp.domain.entities.ResultConvert

object CurrencyRepository {

    private val service = RetrofitInstance.getService()

    suspend fun getFluctuation(startDate: String, endDate: String): Rates {
        return service.getFluctuation(startDate, endDate)
    }

    suspend fun convert(to: String, from: String, amount: String): ResultConvert {
        return service.convert(to, from, amount)
    }
}
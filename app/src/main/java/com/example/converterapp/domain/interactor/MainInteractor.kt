package com.example.converterapp.domain.interactor

import com.example.converterapp.domain.usecases.GetConvertionResultUseCase
import com.example.converterapp.domain.usecases.GetCurrenciesUseCase

class MainInteractor{

    private val getCurrenciesUseCase = GetCurrenciesUseCase()
    private val getConvertionResultUseCase = GetConvertionResultUseCase()

    suspend fun getListCurrencies() = getCurrenciesUseCase.getCurrencies("2023-05-23", "2023-05-24")

    suspend fun getConvertion(to: String, from: String, amount: String) = getConvertionResultUseCase.convert(to, from, amount)
}
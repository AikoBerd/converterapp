package com.example.converterapp.domain.entities

data class ResultConvert(
    val result: Double,
    val success: Boolean
)
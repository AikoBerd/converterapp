package com.example.converterapp.domain.entities

import androidx.annotation.ColorRes

data class Currency(
    val name: String,
    val change: Double,
    val change_pct: Double,
    val end_rate: Double,
    val start_rate: Double,
    @ColorRes val backgroundColorResId: Int
)
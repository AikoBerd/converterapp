package com.example.converterapp.presentation.mapper

import com.example.converterapp.R
import com.example.converterapp.domain.entities.Currency

class CurrencyMapper {
    fun mapToPresentation(rates: Map<String, Currency>): List<Currency> {
        return rates.entries.map { entry ->
            val currencyCode = entry.key
            val currency = entry.value

            val backgroundColorResId = if (currency.change >= 0) {
                R.color.green
            }else {
                R.color.red
            }

            Currency(
                name = currencyCode,
                change = currency.change,
                change_pct = currency.change_pct,
                end_rate = currency.end_rate,
                start_rate = currency.start_rate,
                backgroundColorResId = backgroundColorResId
            )
        }
    }
}

package com.example.converterapp.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.converterapp.domain.interactor.MainInteractor
import com.example.converterapp.domain.entities.Currency
import com.example.converterapp.domain.entities.ResultConvert
import kotlinx.coroutines.launch

class CurrencyViewModel : ViewModel(){

    private val interactor = MainInteractor()
    private val _currencies = MutableLiveData<Map<String, Currency>>()
    val currencies: LiveData<Map<String, Currency>> = _currencies

    private val _resultConvert = MutableLiveData<ResultConvert>()
    val resultConvert: LiveData<ResultConvert> = _resultConvert

    fun getFluctuation(){
        viewModelScope.launch {
            _currencies.value = interactor.getListCurrencies().rates
        }
    }

    fun convert(to: String, from: String, amount: String){
        viewModelScope.launch {
            _resultConvert.value = interactor.getConvertion(to, from, amount)
        }
    }
}
package com.example.converterapp.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.converterapp.R
import com.example.converterapp.domain.interactor.MainInteractor
import com.example.converterapp.domain.repository.CurrencyRepository
import com.example.converterapp.domain.usecases.GetConvertionResultUseCase
import com.example.converterapp.domain.usecases.GetCurrenciesUseCase

class ConvertCurrencyFragment : Fragment() {

    private val viewModel: CurrencyViewModel by viewModels()

    private lateinit var convertCurrencyBtn: Button
    private lateinit var resultCurrency: TextView
    private lateinit var fromCurrency: EditText
    private lateinit var toCurrency: EditText
    private lateinit var amount: EditText

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_convert_currency, container, false)
        convertCurrencyBtn = view.findViewById(R.id.convertCurrencyBtn)
        resultCurrency = view.findViewById(R.id.resultCurrency)
        fromCurrency = view.findViewById(R.id.fromCurrency)
        toCurrency = view.findViewById(R.id.toCurrency)
        amount = view.findViewById(R.id.amount)

//        viewModel = ViewModelProvider(
//            this,
//            CurrencyViewModelFactory(MainInteractor(GetCurrenciesUseCase(CurrencyRepository), GetConvertionResultUseCase(CurrencyRepository)))
//        )[CurrencyViewModel::class.java]

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        convertCurrencyBtn.setOnClickListener {
            val fromCurrencyText = fromCurrency.text.toString()
            val toCurrencyText = toCurrency.text.toString()
            val amountText = amount.text.toString()

            viewModel.convert(toCurrencyText, fromCurrencyText, amountText)
        }

        viewModel.resultConvert.observe(viewLifecycleOwner) { result ->
            resultCurrency.text = result.result.toString()
        }
    }
}
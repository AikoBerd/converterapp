package com.example.converterapp.data.retrofit

import com.example.converterapp.domain.entities.Rates
import com.example.converterapp.domain.entities.ResultConvert
import retrofit2.http.*

interface CurrencyService {
    @Headers("apikey: 5LKh95EeIWFEpesWenKMlThryPgw0DzD")
    @GET("fluctuation")
    suspend fun getFluctuation(@Query("start_date") startDate: String, @Query("end_date") endDate: String): Rates

    @Headers("apikey: 5LKh95EeIWFEpesWenKMlThryPgw0DzD")
    @GET("convert")
    suspend fun convert(@Query("to") to: String, @Query("from") from: String, @Query("amount") amount: String): ResultConvert
}
